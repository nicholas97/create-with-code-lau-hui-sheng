﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRB;
    public float jumpForce;
    public float gravityModifier;
    private bool isOnGround = true;
    public bool gameOver = false;

    public ParticleSystem particleSystem;
    public ParticleSystem dirtyParticle;

    public AudioClip jumpSound;
    public AudioClip clashSound;

    private Animator playerAnimate;

    private AudioSource playerAudio;

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        Physics.gravity *= gravityModifier;

        playerAnimate = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();


        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround && !gameOver)
        {
            dirtyParticle.Stop();
            playerAnimate.SetTrigger("Jump_trig");
            playerRB.AddForce(Vector3.up*jumpForce,ForceMode.Impulse);
            isOnGround = false;
            playerAudio.PlayOneShot(jumpSound, 1.0f);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Ground"))
        {
            dirtyParticle.Play();
            isOnGround = true;
        } else if (collision.gameObject.CompareTag("Obstacle"))
        {
            particleSystem.Play();
            dirtyParticle.Stop();
            gameOver = true;
            Debug.Log("Game Over");

            playerAnimate.SetBool("Death_b",true);
            playerAnimate.SetInteger("DeadthType",2);

            playerAudio.PlayOneShot(clashSound,1.0f);
        }

    }
}
