﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    private float zBoundary = 16.0f;
    private float startInterval = 2.0f;
    private float delayInterval = 1.0f;


    public GameObject[] enemies;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandomEnemy",startInterval,delayInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnRandomEnemy()
    {
        int index = Random.Range(0,enemies.Length);
        Instantiate(enemies[index],GenerateRandomLocation(),enemies[index].transform.rotation);
    }

    private Vector3 GenerateRandomLocation()
    {
        Vector3 ranLocation = new Vector3(-10,0.6f,Random.Range(-zBoundary,zBoundary));
        return ranLocation;
    }
}
