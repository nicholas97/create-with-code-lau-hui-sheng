﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float xDestroy = 10;
    public float speed= 5.0f;
    private Rigidbody objectRB;

    // Start is called before the first frame update
    void Start()
    {
        objectRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        objectRB.AddForce(Vector3.right * speed);

        if (transform.position.x > xDestroy)
        {
            Destroy(gameObject);
        }
    }
}
